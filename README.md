
# Karray and Symbolx:
## Optimization variables into python objects for efficient math operations and scenario analysis

Vienna, 23-03-2023, at [OpenMod 2023 workshop](https://forum.openmod.org/tag/vienna-2023)

Author: [Carlos Gaete](https://www.carlosgaete.com/)

Repo: https://gitlab.com/diw-evu/tutorials/symbolx_openmod2023.git

This tutorial is intended to show the new two python packages: Symbolx and Karray. Symbolx aims to simplify mathematical operations between resulting variables from optimization problems. Scenarios analysis and quick data visualization are the tool’s focus. Symbolx helps to keep track of variables and their dimensions when considering several scenarios. It also enables the user to make mathematical operations between variables with the support of Karray. Symbolx can work with several file formats, such as GAMS-GDX, arrow tables, and CSV. Custom parsers can be easily incorporated to read other file formats.

Karray is a simple tool that intends to abstract the users from the complexity of working with labelled multi-dimensional arrays. Numpy is the tool’s core, with an extensive collection of high-level mathematical functions to operate on multi-dimensional arrays efficiently thanks to its well-optimized C code. With Karray, we put effort into generating lightweight objects expecting to reduce overheads and avoid large loops that cause bottlenecks and impact performance. Numpy is the only relevant dependency, while Polars, Pandas and Pyarrow are required to import, export and store the arrays.


## This tutorial is organized in four parts:

- Part 1: we present karray package and we compare with xarray package.
- Part 2: we run a toy optimization problem to generate two scenarios and we save the resulting variables in CSV files.
- Part 3: we present symbolx package whose main dependency is karray. We show the key functionality of loading the resulting files of several scenarios.
- Part 4: we describe helpful methods in symbolx by using variables with 4 and 5 dimensions

In the end, we expect to have time to discuss cases where you may see that karray and symbolx could be useful.

## Instalation:

1. Create a conda environment with python 3.10
2. Activate such environment in the terminal or command-line
3. Install with pip the following packages
    - symbolx==0.3.2
    - plotly
    - polars
    - pyomo
    - jupyter
4. Install with conda the solver glpk:
    - `conda install -c conda-forge glpk`

5. run jupyter notebook or VSCode with jupyter extension and open the Part_1 ipynb file.