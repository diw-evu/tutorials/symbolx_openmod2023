{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Karray and Symbolx:\n",
    "## Optimization variables into python objects for efficient math operations and scenario analysis\n",
    "\n",
    "Vienna, 23-03-2023, at [OpenMod 2023 workshop](https://forum.openmod.org/tag/vienna-2023)\n",
    "\n",
    "Author: [Carlos Gaete](https://www.carlosgaete.com/)\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial is intended to show the new two python packages: Symbolx and Karray. Symbolx aims to simplify mathematical operations between resulting variables from optimization problems. Scenarios analysis and quick data visualization are the tool’s focus. Symbolx helps to keep track of variables and their dimensions when considering several scenarios. It also enables the user to make mathematical operations between variables with the support of Karray. Symbolx can work with several file formats, such as GAMS-GDX, arrow tables, and CSV. Custom parsers can be easily incorporated to read other file formats.\n",
    "\n",
    "Karray is a simple tool that intends to abstract the users from the complexity of working with labelled multi-dimensional arrays. Numpy is the tool’s core, with an extensive collection of high-level mathematical functions to operate on multi-dimensional arrays efficiently thanks to its well-optimized C code. With Karray, we put effort into generating lightweight objects expecting to reduce overheads and avoid large loops that cause bottlenecks and impact performance. Numpy is the only relevant dependency, while Polars, Pandas and Pyarrow are required to import, export and store the arrays."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial is organized in four parts:\n",
    "\n",
    "- Part 1: we present karray package and we compare with xarray package.\n",
    "- Part 2: we run a toy optimization problem to generate two scenarios and we save the resulting variables in CSV files.\n",
    "- Part 3: we present symbolx package whose main dependency is karray. We show the key functionality of loading the resulting files of several scenarios.\n",
    "- Part 4: we describe helpful methods in symbolx by using variables with 4 and 5 dimensions\n",
    "\n",
    "In the end, we expect to have time to discuss cases where you may see that karray and symbolx could be useful."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "import os"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 1: Karray"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We import karray, pandas, polars and xarray\n",
    "\n",
    "You can install karray by typing in the terminal or command line the following:\n",
    "\n",
    "```Bash\n",
    "$ pip install karray==0.2.1\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import karray as ka\n",
    "\n",
    "import pandas as pd\n",
    "import polars as pl\n",
    "import xarray as xr"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We create an index dictionary that contains the dimensions names as string and a list of dimension elements\n",
    "value is a one dimension list that contains the corresponding values that can be integers or floats."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index = {\n",
    "    'origin':['Canada','Canada','Brazil', 'Brazil'],\n",
    "    'fruit':['apple','apple','orange','orange'],\n",
    "    'box'  :[1,2,3,4]\n",
    "    }\n",
    "\n",
    "value = [100, 200, 300, 400]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## karray.Array creation from data tuple"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock = ka.Array(data=(index,value))\n",
    "stock"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock.long"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock.long.index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock.long.value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock.dense"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(stock.dense)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock.dense.ndim"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock.dims"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stock.capacity"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also include coordinates. It is a dictionary that contains dimension names and all coordinates of a dimension. Index dimension elements are a subset of the coordinates. Coordinates are a list of unique values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coordinates = {\"origin\":[\"Canada\",\"Brazil\"],\"fruit\":[\"apple\",\"orange\",\"banana\",\"mango\"], \"box\":[1,2,3,4,5]}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ka_stock = ka.Array(data=(index,value), coords=coordinates)\n",
    "ka_stock"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ka_stock.coords"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ka_stock.capacity"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get pandas dataframe from Array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pandasdf = ka_stock.to_pandas()\n",
    "pandasdf"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Array from pandas dataframe\n",
    "\n",
    "we can create an Array from pandas or polars dataframe and optionaly providing the coordinates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd_stock = ka.from_pandas(df=pandasdf, coords=coordinates)\n",
    "pd_stock"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Getting a polars dataframe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "polardf = pl.from_pandas(pandasdf)\n",
    "polardf"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Array from polars dataframe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pl_stock = ka.from_polars(df=polardf, coords=coordinates)\n",
    "pl_stock"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A comparison with Xarray\n",
    "\n",
    "we create a xarray DataArray from pandas dataframe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_stock = xr.DataArray(pandasdf)\n",
    "xr_stock"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But it take the dataframe as a 2 dimensions array.\n",
    "\n",
    "So, now we are going to create xarray from karray by providing the dense and coords attributes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_stock = xr.DataArray(data=ka_stock.dense, coords=ka_stock.coords)\n",
    "xr_stock"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simple operations with karray\n",
    "\n",
    "We already have an array called ka_stock. We are going to create a new array that represent the price of some fruits, but orange price is not included."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index_price = {\n",
    "    'origin':['Canada','Brazil','Brazil'],\n",
    "    'fruit':['apple','mango','banana'],\n",
    "    }\n",
    "\n",
    "value_price = [0.1, 0.2, 0.3]\n",
    "\n",
    "ka_price_wo_orange = ka.Array(data=(index_price,value_price))\n",
    "ka_price_wo_orange"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We perform the multiplication of two arrays. Karray takes care of matching the coordinates. As we multiply quantities with prices, we expect to get the bill."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ka_bill = ka_stock*ka_price_wo_orange\n",
    "ka_bill"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ka_bill only returns values for coordinates (Canada,apple,1) and (Canada,apple,2). However, if we return the dataframe we will see all remaining combinations are also present with zero (below)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ka_bill.to_pandas()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename=os.path.join('image','karray.png'), width=350)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We create now an array that includes (Brazil,orange) price"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index_price_orange = {\n",
    "    'origin':['Brazil'],\n",
    "    'fruit':['orange'],\n",
    "    }\n",
    "\n",
    "value_price_orange = [0.4]\n",
    "\n",
    "ka_price_orange = ka.Array(data=(index_price_orange,value_price_orange))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Addition and multiplication"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ka_bill_ok = ka_stock*ka_price_wo_orange + ka_stock*ka_price_orange"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ka_bill_ok"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Xarray operations and differences with karray\n",
    "\n",
    "we create now two arrays of prices with and without orange"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_price_wo_orange = xr.DataArray(data=ka_price_wo_orange.dense, coords=ka_price_wo_orange.coords)\n",
    "xr_price_orange = xr.DataArray(data=ka_price_orange.dense, coords=ka_price_orange.coords)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_stock*xr_price_wo_orange"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_stock"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename=os.path.join('image','karray_xarray.png'), width=500)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Addition and multiplication"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_bill = xr_stock*xr_price_wo_orange + xr_stock*xr_price_orange\n",
    "xr_bill"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_stock*xr_price_wo_orange"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_stock*xr_price_orange"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename=os.path.join('image','empty1.png'), width=500)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "tuto",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
